﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.Entities;
using ROKO.Library.FakeDAL;

namespace ROKO.Library.FakeDAL.Interface
{
    public interface ICatalogDAO<T> where T : LibraryObject
    {
        int Add(T unit);

        bool Remove(int id);

        IEnumerable<LibraryObject> GetCatalog();

        IEnumerable<LibraryObject> GetRecordByTitle(string title);

        IEnumerable<LibraryObject> Sort(SortingCriterion criterion);

        IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author);

        IEnumerable<LibraryObject> FindItemsByAuthor(Author author);

        Dictionary<int, List<LibraryObject>> GroupRecordsByYear();
    }
}
