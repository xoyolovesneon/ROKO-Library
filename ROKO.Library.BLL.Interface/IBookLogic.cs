﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.Entities;

namespace ROKO.Library.BLL.Interface
{
    public interface IBookLogic
    {
        IEnumerable<Book> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate);
    }
}
