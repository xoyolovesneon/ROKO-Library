﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.Entities;

namespace ROKO.Library.BLL.Interface
{
    public interface ICatalogLogic<T> where T : LibraryObject
    {
        int Add(T unit);

        bool Remove(int id);

        IEnumerable<LibraryObject> GetCatalog();

        IEnumerable<LibraryObject> GetRecordByTitle(string title);

        IEnumerable<LibraryObject> Sort(SortingCriterion criterion);

        IEnumerable<LibraryObject> FindItemsByAuthor(Author author);

        IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author);

        Dictionary<int, List<LibraryObject>> GroupRecordsByYear();
    }
}
