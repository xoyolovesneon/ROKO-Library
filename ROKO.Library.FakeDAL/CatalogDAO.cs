﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.Entities;
using ROKO.Library.FakeDAL.Interface;

namespace ROKO.Library.FakeDAL
{
    public class CatalogDAO : ICatalogDAO<LibraryObject> 
    {
        private static List<LibraryObject> _catalog = new List<LibraryObject>(0);

        private static int _catalogId = 0;

        public int Add(LibraryObject unit)
        {
            unit.Id = _catalogId;
            _catalogId++;
            _catalog.Add(unit);
            
            return _catalogId;
        }

        public bool Remove(int id)
        {
            var item = _catalog.FirstOrDefault(p => p.Id == id);
            if (item != null)
            {
                _catalog.Remove(item);
                return true;
            }
            return false;
        }

        public IEnumerable<LibraryObject> Sort(SortingCriterion criterion)
        {
            return criterion.Equals(0) ? AscSort() : DescSort();
        }

        public IEnumerable<LibraryObject> AscSort()
        {
            return _catalog.OrderBy(n => n.PublicationOrApplicationYear);
        }

        public IEnumerable<LibraryObject> DescSort()
        {
            return AscSort().Reverse();
        }

        public IEnumerable<LibraryObject> GetCatalog()
        {
            return _catalog;
        }

        public IEnumerable<LibraryObject> GetRecordByTitle(string inputTitle)
        {
            return _catalog.Where(p => p.Title.Contains(inputTitle)); ; 
        }

        public Dictionary<int, List<LibraryObject>> GroupRecordsByYear()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LibraryObject> FindItemsByAuthor(Author author)
        {
            return _catalog.OfType<Book>().Where(n => n.Author.Contains(author));

            //TODO:
        }

        public IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author)
        {
            var resultBook = _catalog.OfType<Book>().Where(n => n.Author.Contains(author));
            var resultPatent = _catalog.OfType<Patent>().Where(n => n.Inventor.Contains(author));

            return (IEnumerable<LibraryObject>)resultBook.Zip(resultPatent); 
        }
    }
}
