﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.Entities;
using ROKO.Library.FakeDAL.Interface;

namespace ROKO.Library.FakeDAL
{
    public class BookDAO : CatalogDAO, IBookDAO
    {
        public IEnumerable<Book> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate)
        {
            var temp = GetCatalog().Where(n => n is Book);

            List<Book> result = new List<Book>(0);

            foreach (var item in temp)
            {
                if (item.Title.StartsWith(publisherNameTemplate))
                {
                    result.Add(item as Book);
                }    
            }
          
            return (IEnumerable<Book>)result.OfType<Book>().GroupBy(n => n.PublishingHouse);
        }
    }
}
