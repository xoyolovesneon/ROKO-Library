﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ROKO.Library.BLL
{
    public class Validator
    {
        public const int FirstNameMaxLength = 50;
        public const int LastNameMaxLength = 200;
        public const int PlaceNameMaxLength = 200;
        public const int TitleMaxLength = 300;
        public const int PublisherNameMaxLength = 300;
        public const int NoteMaxLength = 2000;

        public bool IsFirstUpper(string input)
        {
            var startSymbol = input[0];

            return Char.ToUpper(startSymbol) == startSymbol;
        }

        public bool IsHyphenOrApostropheFirstOrLast(string input)
        {
            return !(input[0] == '-' || input[input.Length - 1] == '-') || (input[0] == '\'' || input[input.Length - 1] == '\'');
        }

        public bool IsUpperAfterHyphenOrApostrophe(string input)
        {
            for (int i = 1; i < input.Length - 2; i++)
            {
                if ((input[i] == '-' || input[i] == '\'') && (char.IsUpper(input[i + 1])))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ISBNChecker(string input)
        {
            Regex isbn = new Regex(@"\b(ISBN)\b\s([0-7]|8[0-9]|9[0-4]|9[5-8][0-9]|99[0-3]|99[4-8][0-9]|999[0-9][0-9])-\d{1,7}-\d{1,7}-[0-9|X]");

            return (input.Length == 18 && isbn.IsMatch(input));
        }

        public bool ISSNChecker(string input)
        {
            Regex issn = new Regex(@"\b[I][S][S][N]\b\s[0-9]{4}-[0-9]{4}");

            return (input.Length == 14 && issn.IsMatch(input));
        }

        public bool PublishingYearChecker(int pubYear)
        {
            int currentYear = DateTime.Now.Year;

            return !(pubYear > currentYear);
        }

        public bool IsOnlyOneLanguage(string input)
        {
            string russianAlphabet = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            char[] rusAlpCharArray = russianAlphabet.ToCharArray();
            string latinAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            char[] latinAlpCharArray = latinAlphabet.ToCharArray();

            bool result = false;

            if (input.IndexOfAny(rusAlpCharArray) != -1)
            {
                if (input.IndexOfAny(latinAlpCharArray) != -1)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }

            return result;
        }

    }
}
