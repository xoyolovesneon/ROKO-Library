﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.BLL.Interface;
using ROKO.Library.Entities;
using ROKO.Library.FakeDAL;
using ROKO.Library.FakeDAL.Interface;

namespace ROKO.Library.BLL
{
    public class CatalogLogic<T> : ICatalogLogic<T> where T : LibraryObject
    {
        private readonly ICatalogDAO<LibraryObject> _catalogDAO;

        public int Add(T unit)
        {
            return _catalogDAO.Add(unit);
        }
        public bool Remove(int id)
        {
            return _catalogDAO.Remove(id);
        }

        public IEnumerable<LibraryObject> Sort(SortingCriterion criterion)
        {
            return _catalogDAO.Sort(criterion);
        }

        public IEnumerable<LibraryObject> FindBooksAndPatentsOfAuthor(Author author)
        {
            return _catalogDAO.FindBooksAndPatentsOfAuthor(author);
        }

        public IEnumerable<LibraryObject> GetCatalog()
        {
            return _catalogDAO.GetCatalog();
        }

        public IEnumerable<LibraryObject> GetRecordByTitle(string title)
        {
            return _catalogDAO.GetRecordByTitle(title);
        }

        public Dictionary<int, List<LibraryObject>> GroupRecordsByYear()
        {
            return _catalogDAO.GroupRecordsByYear();
        }

        public IEnumerable<LibraryObject> FindItemsByAuthor(Author author)
        {
            return _catalogDAO.FindBooksAndPatentsOfAuthor(author);
        }
    }
}
