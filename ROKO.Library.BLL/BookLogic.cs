﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ROKO.Library.BLL.Interface;
using ROKO.Library.Entities;
using ROKO.Library.FakeDAL.Interface;

namespace ROKO.Library.BLL
{
    public class BookLogic : IBookLogic
    {
        private readonly IBookDAO _bookDAO;

        public IEnumerable<Book> FindAllByPublisherNameTemplateAndGroupByPublisher(string publisherNameTemplate)
        {
            return _bookDAO.FindAllByPublisherNameTemplateAndGroupByPublisher(publisherNameTemplate);
        }
    }
}
