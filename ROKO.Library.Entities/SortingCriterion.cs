﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROKO.Library.Entities
{
    public class SortingCriterion
    {
        public enum SortCriterion
        {
            ASCPUBLICATIONYEAR = 0,
            DESCPUBLICATIONYEAR = 1
        }
    }
}
