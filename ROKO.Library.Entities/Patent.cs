﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROKO.Library.Entities
{
    public class Patent : LibraryObject
    {
        private DateTime _applicationDate;
        public List<Author> Inventor { get; set; }

        public string Country { get; set; }

        public int RegistrationNumber { get; set; }

        public DateTime ApplicationDate {
            get => _applicationDate;
            set  
            { 
                PublicationOrApplicationYear = value.Year;
                _applicationDate = value;
            }
        }

        public DateTime PublicationDate { get; set; }

        public Patent(string title, List<Author> inventor, string country, int regNumber, DateTime appDate, DateTime pubDate, int numOfPages, string note)
        {
            Title = title;
            Inventor = inventor;
            Country = country;
            RegistrationNumber = regNumber;
            ApplicationDate = appDate;
            PublicationDate = pubDate;
            NumberOfPages = numOfPages;
            Note = note;
        }



        public override string ToString()
        {
            return $"{Id} | {Title} | {Inventor} | {Country} | {RegistrationNumber} | {ApplicationDate} | {PublicationDate} | {NumberOfPages} | {Note}";
        }
    }
}
