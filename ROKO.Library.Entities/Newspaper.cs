﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROKO.Library.Entities
{
    public class Newspaper : LibraryObject
    {
        public string PlaceOfPublication { get; set; }

        public string PublishingHouse { get; set; }

        //public int PublicationYear { get; set; }

        public int IssueNumber { get; set; }

        public DateTime ReleaseDate { get; set; }

        public string ISSN { get; set; }

        public Newspaper(string title, string placeOfPub, string pubHouse, int pubYear, int numOfPages, string note, int issueNumber, DateTime releaseDate, string issn)
        {
            Title = title;
            PlaceOfPublication = placeOfPub;
            PublishingHouse = pubHouse;
            PublicationOrApplicationYear = pubYear;
            NumberOfPages = numOfPages;
            Note = note;
            IssueNumber = issueNumber;
            ReleaseDate = releaseDate;
            ISSN = issn;
        }

        public override string ToString()
        {
            return $"{Id} | {Title} | {PlaceOfPublication} | {PublishingHouse} | {PublicationOrApplicationYear} | {NumberOfPages} | {Note} | {IssueNumber} | {ReleaseDate} | {ISSN}";
        }
    } 
}
