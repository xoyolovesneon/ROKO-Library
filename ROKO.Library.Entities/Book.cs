﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROKO.Library.Entities
{
    public class Book : LibraryObject
    {
        public List<Author> Author { get; set; }

        public string PlaceOfPublication { get; set; }

        public string PublishingHouse { get; set; }

        //public int PublicationYear { get; set; }

        public string ISBN { get; set; }

        public Book(string title, List<Author> author, string placeOfPub, string pubHouse, int pubYear, int numOfPages, string note, string isbn)
        {
            Title = title;
            Author = author;
            PlaceOfPublication = placeOfPub;
            PublishingHouse = pubHouse;
            PublicationOrApplicationYear = pubYear;
            NumberOfPages = numOfPages;
            Note = note;
            ISBN = isbn;
        }

        public override string ToString()
        {
            return $"{Id} | {Title} | {Author} | {PlaceOfPublication} | {PublishingHouse} | {PublicationOrApplicationYear} | {NumberOfPages} | {Note} | {ISBN}";
        }
    }
}
