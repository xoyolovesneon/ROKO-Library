﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ROKO.Library.Entities
{
    public class LibraryObject
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int PublicationOrApplicationYear { get; set; }

        public int NumberOfPages { get; set; }

        public string Note { get; set; }
    }
}
